# TP4

## Getting Started

### Mapping IP addresses to domain names
With the following command, we will add the domains configured in the compose
```bash
$ sudo sh -c 'echo "127.0.0.1 wordpress.local joomla.local mkdocs.local" >> /etc/hosts'
```

### Up
```bash
$ docker compose up -d
```

### Check Health
```bash
$ docker compose ps -a | grep -E 'wdatabase|wordpress|jdatabase|joomla|mkdocs|ingress'
```

### Access
**[Mkdocs](http://mkdocs.local)**  
**[WordPress](http://wordpress.local)**  
**[Joomla](http://joomla.local)**

### Down
```bash
$ docker compose down --remove-orphans -v
```