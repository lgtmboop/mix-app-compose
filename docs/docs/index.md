# Welcome to Compose Challenge
El desafío consiste en crear un compose para ejecutar un joomla con su respectiva base de datos y en el mismo compose deben tener un wordpress con su respectiva base de datos.

- La imagen base a utilizar es wordpress tanto la db como wordpress deben tener volumen persistente para resguardar los cambios.
- La imagen base a utilizar es bitnami/joomla tanto la db como joomla deben tener volumen persistente para resguardar los cambios.
- Publicar los servicios mediante un proxy reverso con dns.

Opcional `Pueden tomar uno o ambos`  

- Crear una documentación sencilla con el procedimiento realizado para crear este desafío usando mkdocs + material for mkdocs (squidfunk/mkdocs-material).
- Publicarla mediante un servidor web (apache/nginx) con un dns propio

## First Challenge

### WordPress & MYSQL
Lo primero que hice fue buscar la imágen de WordPress en DockerHub, utilicé el filtro para que me muestre sólo las oficiales, verificadas y sponsoreadas.  
Como ya existía el proveedor oficial, consumí esa `https://hub.docker.com/_/wordpress`.  
Leí la documentación y ya había un ejemplo con integración de base de datos (me guié de eso y cree las variables de entorno en un .env).

Una vez construídos ambos servicios `wordpress y mysql`, levante el compose, chequee los códigos de estado con `docker compose ps`.  
Si algún servicio no aparecía, utilizaba `docker compose ps -a`.  
Si había algún error, tiraba el comando `docker compose logs <service-name> -f` (en algunos casos me apoye de ChatGPT para entender errores o logs extraños si aparecían).

### JOOMLA & MARIADB
Lo segundo fue buscar la imágen en DockerHub, nuevamente utilizando el filtro para que muestre sólo las oficiales, verificadas y sponsoreadas.  
Como la que se sugirió era de un publicador verificado, la consumí.  

Nuevamente ya existía un ejemplo con integración de base de datos (me guié de eso y agregué las variables de entorno en el .env ya existente).  

Una vez que construí ambos servicios `joomla y mariadb`, levante el compose y chequee los códigos de estado.  
Repito, si algún servicio no aparecía, utilizaba `docker compose ps -a`.
Si había algún error, tiraba el comando `docker compose logs <service-name> -f` (en algunos casos también me apoye de ChatGPT para entender errores o logs extraños si aparecían).  

Acá destaco que he visto un error en el cual el contenedor que levantaba el servicio de Joomla, no se podía conectar a la base de datos, así que chequee la network
con `docker network ls` y `docker network inspect <network-id>`.  

Al final había olvidado setear una variable de entorno que no estaba en el ejemplo en la documentación que tome, tuve que meterme a ver un compose armado por ellos en GitHub.

### Ingress
Por último utilicé la imágen que vimos para aprender el tema de "Reverse Proxy".  
Configuré el servicio como un **ingress** y setee las variables de entorno `VIRTUAL_HOST` y `VIRTUAL_PORT` para los servicios web.  
En el caso del servicio **wordpress**, no definí **VIRTUAL_PORT** porque el puerto expuesto era el 80 (no lo hice porque se resuelve de forma implícita).
